import 'dotenv/config';
import express from 'express';
import { apiRouter } from './api';

const PORT = process.env.PORT ?? 3000;

const app = express();

app.use((req, _, next) => {
	console.log(`${req.method} ${req.url}`);
	next();
});
app.use('/api', apiRouter);
app.use(express.static('public'));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
