import { array, object, string, z } from 'zod';
import * as fs from 'fs/promises';

export const Tweet = object({
	title: string(),
});
export type Tweet = z.infer<typeof Tweet>;

const TWEETS_FILE = './db/tweets.json';

export function getAllTweets(): Promise<Tweet[]> {
	return fs.readFile(TWEETS_FILE)
		.then(res => JSON.parse(res.toString()))
		.then(array(Tweet).parse)
		.catch(() => []);
}

export function writeTweets(tweets: Tweet[]) {
	return fs.writeFile(TWEETS_FILE, JSON.stringify(tweets));
}

export function addTweet(tweet: Tweet) {
	return getAllTweets()
		.then(tweets => [tweet, ...tweets])
		.then(writeTweets);
}
