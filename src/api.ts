import express, { Router } from 'express';
import { addTweet, getAllTweets, Tweet, writeTweets } from './tweets';
import { createAtomicPromise } from './atomic-promise';

export const apiRouter = Router()
	.use(express.json())
	.get('/tweets', (_, res) =>
		getAllTweets()
			.then(tweets => tweets.slice(0, 100))
			.then(res.json.bind(res))
			.catch(() => res.sendStatus(500)))
	.get('/tweets/all', (_, res) =>
		getAllTweets()
			.then(res.json.bind(res))
			.catch(() => res.sendStatus(500)))
	.post('/tweets', (req, res) =>
		createAtomicPromise(() =>
			addTweet(Tweet.parse(req.body))
				.then(() => res.sendStatus(201))
				.catch(() => res.sendStatus(500))))
	.post('/tweets/double', (req, res) =>
		createAtomicPromise(() =>
			getAllTweets()
				.then(tweets => tweets.concat(tweets))
				.then(writeTweets)
				.then(() => res.sendStatus(201))
				.catch(() => res.sendStatus(500))));
