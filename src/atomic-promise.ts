let atomicPromise: Promise<unknown> = Promise.resolve(null);

export async function createAtomicPromise<T>(createPromise: () => Promise<T>): Promise<T> {
	const promise = atomicPromise.then(createPromise);

	atomicPromise = promise;

	return promise;
}
