const tweetForm = document.getElementById('tweet-form');
const tweetDoubleForm = document.getElementById('tweet-double-form');
const tweetsSection = document.getElementById('tweets');

function renderTweets() {
	tweetsSection.textContent = "Loading ...";
	fetch('/api/tweets')
		.then(res => res.json())
		.then(tweets => {
			tweetsSection.innerHTML = "";

			for (const tweet of tweets) {
				const article = document.createElement('article');
				article.innerText = tweet.title;

				tweetsSection.appendChild(article);
			}
		});
}

renderTweets();

tweetForm
	?.addEventListener('submit', e => {
		e.preventDefault();

		fetch('/api/tweets', {
			method: 'post',
			body: JSON.stringify({
				title: e.target?.elements.title.value,
			}),
			headers: {
				'Content-Type': 'application/json',
			}
		})
			.then(() => tweetForm.reset())
			.then(renderTweets);
	});

tweetDoubleForm
	?.addEventListener('submit', e => {
		e.preventDefault();

		fetch('/api/tweets/double', { method: 'post', body: null })
			.then(renderTweets);
	});
