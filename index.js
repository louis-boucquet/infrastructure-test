const fs = require('fs/promises')

const fileName = 'test.json';

let count = 0;
let contents = [];

const intercalId = setInterval(() => {
	createAtomicPromise(() => fs.readFile(fileName)
		.then(file => {
			const text = file.toString();
			contents.push(text)
			return JSON.parse(text);
		})
		.then(dates => {

			dates.push(new Date().toISOString());
			count += 1;

			return fs.writeFile(fileName, JSON.stringify(dates));
		}));
}, 0);
setTimeout(() => {
	clearInterval(intercalId);

	setTimeout(() => {
		console.log(contents);
		console.log(count);
	}, 1000);
}, 10);
